
Database Theory
===================
Created By [Efthaqur Alam](http://efthaqur.mynetwall.info/)

Date 02 November 2016
----------
> MD EFTEQUARUL ALAM   SEIP 137008 Batch - B35   Web Application PHP

Session 21
-------------

**PDO Basic 3**

> **Note:**

> - **Session 20 topics**
> - PDO Basic
> - The PHP Data Objects (PDO) extension defines a lightweight, consistent interface for accessing databases in PHP. Each database driver that implements the PDO interface can expose database-specific features as regular extension functions. Note that you cannot perform any database functions using the PDO extension by itself; you must use a database-specific PDO driver to access a database server.
