<?php

namespace App\Model;

//print_r(PDO::getAvailableDrivers());


use PDO;
use PDOException;


class Database
{
    public $DBH;
    public $host = "localhost";
    public $dbname = "atomic_project_b35";
    public $user = "root";
    public $password = "root";

    public function __construct()
    {

        try {
            # MySQL with PDO_MYSQL
            $this->DBH = new PDO("mysql:host=$this->host;dbname=$this->dbname", $this->user, $this->password);
            echo "Connection Succesfull!";
            $this->DBH->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);


            # UH-OH! Typed DELECT instead of SELECT!

            $this->DBH->prepare('DELECT name FROM people');

        } catch (PDOException $e) {

            echo "I'm sorry, Dave. I'm afraid I can't do that.";

            file_put_contents('PDOErrors.txt', $e->getMessage().PHP_EOL, FILE_APPEND);

        }
    }
}

$STH = new Database;
$STH = $DBH->prepare("INSERT INTO mytable ( name ) values ( 'Efthaqur Alam') (id) values (101) (age) VALUES (24)");

$STH->execute();